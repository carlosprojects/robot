var cli = require('../src/input-validations.js');

describe('Input validations', function() {

    it("Validate arguments X", function() {
        var error = false;
        try {
            var args = {
                x : "test",
                y : 1,
                f : "NORTH"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = e.message == "Argument X is not a valid integer";
        }
        expect(error).toEqual(true);
    });

    it("Validate arguments X is a numeric String", function() {
        var error = false;
        try {
            var args = {
                x : "1",
                y : 1,
                f : "NORTH"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = true;
        }
        expect(error).toEqual(false);
    });

    it("Validate arguments X", function() {
        var error = false;
        try {
            var args = {
                x : "test",
                y : 1,
                f : "NORTH"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = e.message == "Argument X is not a valid integer";
        }
        expect(error).toEqual(true);
    });

    it("Validate arguments Y", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : "test",
                f : "NORTH"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = e.message == "Argument Y is not a valid integer";
        }
        expect(error).toEqual(true);
    });

    it("Validate arguments Y is a numeric String", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : "1",
                f : "NORTH"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = true;
        }
        expect(error).toEqual(false);
    });

    it("Validate arguments F is String", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : 1,
                f : 0
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = e.message == "Argument F is not a String";
        }
        expect(error).toEqual(true);
    });

    it("Validate arguments F is not expected value", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : 1,
                f : "something"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = e.message == "Robot can only be placed looking NORTH, SOUTH, EAST, WEST.";
        }
        expect(error).toEqual(true);
    });

    it("Validate arguments F equlas \"north\" is accepted", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : 1,
                f : "north"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = true;
        }
        expect(error).toEqual(false);
    });

    it("Validate arguments F equlas \"south\" is accepted", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : 1,
                f : "north"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = true;
        }
        expect(error).toEqual(false);
    });

    it("Validate arguments F equlas \"east\" is accepted", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : 1,
                f : "north"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = true;
        }
        expect(error).toEqual(false);
    });

    it("Validate arguments F equlas \"west\" is accepted", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : 1,
                f : "north"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = true;
        }
        expect(error).toEqual(false);
    });

    it("Valid arguments F value is not case sensitive \"NORTH\" is accepted", function() {
        var error = false;
        try {
            var args = {
                x : 1,
                y : 1,
                f : "NORTH"
            };
            cli.arePlaceArgumentsValid(args);
        } catch(e) {
            error = true;
        }
        expect(error).toEqual(false);
    });
});
