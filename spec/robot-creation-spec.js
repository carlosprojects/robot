var constants = require('../src/constants.js');
var robot = require('../src/robot.js');

describe('Toy Robot creation and placing', function() {

    it ("A robot object must be created", function () {
        expect(robot.getRobot(0, 0, NORTH) != undefined).toEqual(true);
    });

    // Testing face creation:

    it ("A robot created at 'X: 0, Y: 0, F: NORTH' must report 'X: 0, Y: 0, F: NORTH'", function () {
        expect(robot.getRobot(0, 0, NORTH).report()).toEqual("X: 0, Y: 0, F: NORTH");
    });

    it ("A robot created at 'X: 0, Y: 0, F: SOUTH' must report 'X: 0, Y: 0, F: SOUTH'", function () {
        expect(robot.getRobot(0, 0, SOUTH).report()).toEqual("X: 0, Y: 0, F: SOUTH");
    });

    it ("A robot created at 'X: 0, Y: 0, F: EAST' must report 'X: 0, Y: 0, F: EAST'", function () {
        expect(robot.getRobot(0, 0, EAST).report()).toEqual("X: 0, Y: 0, F: EAST");
    });

    it ("A robot created at 'X: 0, Y: 0, F: WEST' must report 'X: 0, Y: 0, F: WEST'", function () {
        expect(robot.getRobot(0, 0, WEST).report()).toEqual("X: 0, Y: 0, F: WEST");
    });

    it ("A robot created at 'X: 4, Y: 4, F: 12' must throw an Exception with message: 'Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).'", function () {

        var error = false;
        try {
            robot.getRobot(4, 4, 12);
        } catch(e) {
            error = e.message == "Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).";
        }
        expect(error).toEqual(true);
    });

    it ("A robot created at 'X: 4, Y: 4, F: 135' must throw an Exception with message: 'Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).'", function () {

        var error = false;
        try {
            robot.getRobot(1, 4, 135);
        } catch(e) {
            error = e.message == "Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).";
        }
        expect(error).toEqual(true);
    });

    it ("A robot created at 'X: 2, Y: 2, F: 200' must throw an Exception with message: 'Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).'", function () {

        var error = false;
        try {
            robot.getRobot(2, 2, 200);
        } catch(e) {
            error = e.message == "Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).";
        }
        expect(error).toEqual(true);
    });

    it ("A robot created at 'X: 1, Y: 5, F: 359' must throw an Exception with message: 'Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).'", function () {

        var error = false;
        try {
            robot.getRobot(1, 4, 359);
        } catch(e) {
            error = e.message == "Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).";
        }
        expect(error).toEqual(true);
    });

    // Testing positioning:

    it ("A robot created at 'X: 0, Y: 0, F: NORTH' must report 'X: 0, Y: 0, F: NORTH'", function () {
        expect(robot.getRobot(0, 0, NORTH).report()).toEqual("X: 0, Y: 0, F: NORTH");
    });

    it ("A robot created at 'X: 1, Y: 1, F: NORTH' must report 'X: 1, Y: 1, F: NORTH'", function () {
        expect(robot.getRobot(1, 1, NORTH).report()).toEqual("X: 1, Y: 1, F: NORTH");
    });

    it ("A robot created at 'X: 2, Y: 2, F: NORTH' must report 'X: 2, Y: 2, F: NORTH'", function () {
        expect(robot.getRobot(2, 2, NORTH).report()).toEqual("X: 2, Y: 2, F: NORTH");
    });

    it ("A robot created at 'X: 3, Y: 3, F: NORTH' must report 'X: 3, Y: 3, F: NORTH'", function () {
        expect(robot.getRobot(3, 3, NORTH).report()).toEqual("X: 3, Y: 3, F: NORTH");
    });

    it ("A robot created at 'X: 4, Y: 4, F: NORTH' must report 'X: 4, Y: 4, F: NORTH'", function () {
        expect(robot.getRobot(4, 4, NORTH).report()).toEqual("X: 4, Y: 4, F: NORTH");
    });

    // Testing out of bound:

    it ("A robot created at 'X: 5, Y: 5, F: NORTH' must throw an Exception with message: 'Robot cannot be placed out of boundaries. Board is 5x5.'", function () {

        var error = false;
        try {
            robot.getRobot(5, 5, NORTH);
        } catch(e) {
            error = e.message == "Robot cannot be placed out of boundaries. Board is 5x5.";
        }
        expect(error).toEqual(true);
    });

    it ("A robot created at 'X: -1, Y: -1, F: NORTH' must throw an Exception with message: 'Robot cannot be placed out of boundaries. Board is 5x5.'", function () {

        var error = false;
        try {
            robot.getRobot(-1, -1, NORTH);
        } catch(e) {
            error = e.message == "Robot cannot be placed out of boundaries. Board is 5x5.";
        }
        expect(error).toEqual(true);
    });

    it ("A robot created at 'X: 3, Y: 10, F: NORTH' must throw an Exception with message: 'Robot cannot be placed out of boundaries. Board is 5x5.'", function () {

        var error = false;
        try {
            robot.getRobot(3, 10, NORTH);
        } catch(e) {
            error = e.message == "Robot cannot be placed out of boundaries. Board is 5x5.";
        }
        expect(error).toEqual(true);
    });

    it ("A robot created at 'X: 10, Y: 3, F: NORTH' must throw an Exception with message: 'Robot cannot be placed out of boundaries. Board is 5x5.'", function () {

        var error = false;
        try {
            robot.getRobot(10, 3, NORTH);
        } catch(e) {
            error = e.message == "Robot cannot be placed out of boundaries. Board is 5x5.";
        }
        expect(error).toEqual(true);
    });

    // Testing place:

    it ("A Robot created at created in X: 0, Y: 0, F: NORTH, then is placed in X: 4, Y: 4, F: SOUTH", function() {

        var r = robot.getRobot(0, 0, NORTH);
        var report1 = r.report();
        r.place(4, 4, SOUTH);
        var report2 = r.report();

        var testSucceded = report1 == "X: 0, Y: 0, F: NORTH" && report2 == "X: 4, Y: 4, F: SOUTH";
        expect(testSucceded).toEqual(true);
    });
});
