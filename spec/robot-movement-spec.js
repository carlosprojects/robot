var constants = require('../src/constants.js');
var robot = require('../src/robot.js');

describe('Toy Robot movement testing', function() {

    // Testing robot movements:

    it ("A Robot follows commands: PLACE 0, 0, EAST > MOVE > MOVE > REPORT must show 'X: 0, Y: 2, F: EAST'", function () {
        var r = robot.getRobot(0, 0, EAST);
        r.move();
        r.move();
        expect(r.report()).toEqual("X: 2, Y: 0, F: EAST");
    });

    it ("A Robot follows commands: PLACE 0, 0, NORTH > MOVE > REPORT must show 'X: 0, Y: 1, F: NORTH'", function () {
        var r = robot.getRobot(0, 0, NORTH);
        r.move();
        expect(r.report()).toEqual("X: 0, Y: 1, F: NORTH");
    });

    it ("A Robot follows commands: PLACE 0, 0, NORTH > LEFT > REPORT must show 'X: 0, Y: 0, F: WEST'", function () {
        var r = robot.getRobot(0, 0, NORTH);
        r.left();
        expect(r.report()).toEqual("X: 0, Y: 0, F: WEST");
    });

    it ("A Robot follows commands: PLACE 1, 2, EAST > MOVE > MOVE > LEFT > MOVE > REPORT must show 'X: 3, Y: 3, F: NORTH'", function () {
        var r = robot.getRobot(1, 2, EAST);
        r.move();
        r.move();
        r.left();
        r.move();
        expect(r.report()).toEqual("X: 3, Y: 3, F: NORTH");
    });

    it ("A Robot follows commands: PLACE 1, 2, EAST > MOVE > MOVE > LEFT > MOVE > REPORT must show 'X: 3, Y: 3, F: NORTH'", function () {
        var r = robot.getRobot(1, 2, EAST);
        r.move();
        r.move();
        r.left();
        r.move();
        expect(r.report()).toEqual("X: 3, Y: 3, F: NORTH");
    });

    it ("A Robot follows commands: PLACE 0, 0, NORTH > LEFT > MOVE and must complaint 'No way! I will fall'", function () {

        var msg;
        try {
            var r = robot.getRobot(0, 0, NORTH);
            r.left();
            r.move();
        } catch (e) {
            msg = e.message;
        }

        expect(msg).toEqual("No way! I will fall");
    });

    it ("A Robot follows commands: PLACE 4, 4, NORTH > MOVE and must complaint 'No way! I will fall'", function () {

        var msg;
        try {
            var r = robot.getRobot(4, 4, NORTH);
            r.move();
        } catch (e) {
            msg = e.message;
        }

        expect(msg).toEqual("No way! I will fall");
    });

    it ("A Robot follows commands: PLACE 4, 4, NORTH > MOVE > RIGHT > MOVE must complaint 'No way! I will fall' two times", function () {

        var complaints = 0;
        var r = robot.getRobot(4, 4, NORTH);
        try {
            r.move();
        } catch (e) {
            if (e.message == "No way! I will fall") {
                complaints++;
            };
        }
        r.right();
        try {
            r.move();
        } catch (e) {
            if (e.message == "No way! I will fall") {
                complaints++;
            };
        }

        expect(complaints).toEqual(2);
    });

    it ("A Robot follows commands: PLACE 4, 4, NORTH > MOVE > RIGHT > MOVE > RIGHT > MOVE > RIGHT > MOVE > REPORT must complaint 'No way! I will fall' two times and report 'X: 3, Y: 3, F: WEST' at the end", function () {

        var complaints = 0;
        var r = robot.getRobot(4, 4, NORTH);
        try {
            r.move();
        } catch (e) {
            if (e.message == "No way! I will fall") {
                complaints++;
            };
        }
        r.right();
        try {
            r.move();
        } catch (e) {
            if (e.message == "No way! I will fall") {
                complaints++;
            };
        }
        r.right();
        r.move();
        r.right();
        r.move();

        expect(complaints).toEqual(2);
    });
});
