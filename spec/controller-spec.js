var controller = require('../src/controller.js');

describe('Toy Robot Controller', function() {

    it ("No commands", function () {
        var result = controller.execute([]);

        expect(result.commands).toEqual([]);
        expect(result.position).toEqual("");
        expect(result.error).toEqual("");
    });

    it ("Execute commands: PLACE 0,0,NORTH > REPORT", function () {
        var cmds = ["PLACE 0,0,NORTH", "REPORT"];
        var result = controller.execute(cmds);

        expect(result.commands).toEqual(["PLACE 0,0,NORTH", "REPORT"]);
        expect(result.position).toEqual("X: 0, Y: 0, F: NORTH");
        expect(result.error).toEqual("");
    });

    it ("Execute commands: REPORT, MOVE, PLACE 0,0,NORTH > REPORT - (first two are ignored)", function () {
        var cmds = ["REPORT", "MOVE", "PLACE 0,0,NORTH", "REPORT"];
        var result = controller.execute(cmds);

        expect(result.commands).toEqual(["PLACE 0,0,NORTH", "REPORT"]);
        expect(result.position).toEqual("X: 0, Y: 0, F: NORTH");
        expect(result.error).toEqual("");
    });

    it ("Execute commands: PLACE 4,4,NORTH > REPORT", function () {
        var cmds = ["PLACE 4,4,NORTH", "MOVE", "LEFT", "MOVE", "REPORT"];
        var result = controller.execute(cmds);

        expect(result.commands).toEqual(["PLACE 4,4,NORTH", "LEFT", "MOVE", "REPORT"]);
        expect(result.position).toEqual("X: 3, Y: 4, F: WEST");
        expect(result.error).toEqual("");
    });

    it ("Execute commands: PLACE 0,0,SOUTH > LEFT > MOVE > MOVE > MOVE > MOVE > MOVE > REPORT", function () {
        var cmds = ["PLACE 0,0,SOUTH", "LEFT", "MOVE", "MOVE", "MOVE", "MOVE", "MOVE", "REPORT"];
        var result = controller.execute(cmds);

        //Only four MOVE commands are expected to be executed.
        expect(result.commands).toEqual(["PLACE 0,0,SOUTH", "LEFT", "MOVE", "MOVE", "MOVE", "MOVE", "REPORT"]);
        expect(result.position).toEqual("X: 4, Y: 0, F: EAST");
        expect(result.error).toEqual("");
    });

    it ("Execute commands: PLACE 5,7,SOUTH > REPORT", function () {
        var cmds = ["PLACE 5,7,SOUTH", "REPORT"];
        var result = controller.execute(cmds);

        expect(result.commands).toEqual([]);
        expect(result.position).toEqual("");
        expect(result.error).toEqual("Robot cannot be placed out of boundaries. Board is 5x5.");
    });

    it ("Execute commands: PLACE 5,7,SOUTH > REPORT", function () {
        var cmds = ["PLACE 0,4,EAST", "REPORT", "PLACE 4,0,WEST"];
        var result = controller.execute(cmds);

        expect(result.commands).toEqual(["PLACE 0,4,EAST", "REPORT", "PLACE 4,0,WEST"]);
        expect(result.position).toEqual("X: 4, Y: 0, F: WEST");
        expect(result.error).toEqual("");
    });
});
