var vorpal = require('vorpal')();
var constants = require('./src/constants.js');
var validations = require('./src/input-validations.js');
var robot = require('./src/robot.js');
var r;

/**
 * Defines CLI commands.
 */
vorpal.delimiter('robot$').show();
vorpal
    .command('place <x> <y> <f>', 'Put the toy robot on the table in position X,Y and facing NORTH, SOUTH, EAST or WEST')
    .action(function(args, callback) {
        try {
            validations.arePlaceArgumentsValid(args);
            var x = Number(args.x);
            var y = Number(args.y);
            var f = FACE_STRING_TO_NUMBER_MAP.get(args.f.toUpperCase());

            if (r == undefined) {
                r = robot.getRobot(x, y, f);
            } else {
                r.place(x, y, f)
            }
        } catch (e) {
            this.log(e.message);
        }
        callback();
    });
vorpal
    .command('move', 'Move the toy robot one unit forward in the direction it is currently facing')
    .action(function(args, callback) {
        if (r != undefined) {
            try {
                r.move();
            } catch(e) {
                this.log(e.message);
            }
        }
        callback();
    });
vorpal
    .command('left', 'Rotate the robot 90 degrees to the left without changing the position of the robot.')
    .action(function(args, callback) {
        if (r != undefined) {
            r.left();
        }
        callback();
    });
vorpal
    .command('right', 'Rotate the robot 90 degrees to the right without changing the position of the robot.')
    .action(function(args, callback) {
        if (r != undefined) {
            r.right();
        }
        callback();
    });
vorpal
    .command('report', 'Announce the X,Y and F of the robot.')
    .action(function(args, callback) {
        if (r != undefined) {
            this.log(r.report());
        }
        callback();
    });
