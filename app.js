var fs = require('fs');

var fileName = process.argv.slice(2);
if (fileName.length == 0) {
    console.log("Usage: node app.js <input-file>")
    console.log("<input-file> must be placed under 'input' folder.")
} else {
    var controller = require('./src/controller.js');
    controller.execute(fs.readFileSync('input/' + fileName[0], 'utf8').toString().split('\n'));
}
