/**
 * Toy Robot Class and Factory.
 */

var constants = require('./constants.js');

/**
 * Robot Exception.
 *
 * @param {String} Error message.
 */
RobotException = function(message) {
    this.message = message;
    this.name = "RobotException";
};

/**
 * Verifies if robot placing input is valid.

 * @param {Integer} Position in the board x-axis.
 * @param {Integer} Position in the board y-axis.
 * @param {Integer} Indicates where the robot is facing: NORTH, SOUTH, EAST or WEST.
 */
function isValidInput(x, y, f) {
    if (x < 0 || x >= BOARD_SIZE_X || y < 0 || y >= BOARD_SIZE_Y) {
        throw new RobotException("Robot cannot be placed out of boundaries. Board is 5x5.");
    }

    var validFace = [NORTH, SOUTH, EAST, WEST];
    if (validFace.indexOf(f) == -1) {
        throw new RobotException("Robot can only be placed looking NORTH (90), SOUTH (270), EAST (0), WEST (180).");
    }
};

/**
 * Construct Toy Robot object.
 *
 * @param {Integer} Position in the board x-axis.
 * @param {Integer} Position in the board y-axis.
 * @param {Integer} Indicates where the robot is facing: NORTH, SOUTH, EAST or WEST.
 */
exports.getRobot = function(x, y, f) {

    isValidInput(x, y, f);

    var robot = {
        axisX : x,
        axisY : y,
        face : f,
        place : function(x, y, f) {
            isValidInput(x, y, f);
            this.axisX = x;
            this.axisY = y;
            this.face = f;
        },
        report : function() {
            return "X: " + this.axisX + ", Y: " + this.axisY + ", F: " + FACE_NUMBER_TO_OBJECT_MAP.get(this.face).label;
        },
        right: function () {
            if (this.face == 0) {
                this.face = 270;
            } else {
                this.face -= 90;
            }
        },
        left : function() {
            this.face = this.face + 90;
            if (this.face == 360) {
                this.face = 0;
            }
        },
        move : function() {
            if (this.face == EAST && this.axisX >= 0 && this.axisX < (BOARD_SIZE_X - 1)) {
                this.axisX += 1;
            } else if (this.face == NORTH && this.axisY >= 0 && this.axisY < (BOARD_SIZE_Y - 1)) {
                this.axisY += 1;
            } else if (this.face == WEST && this.axisX > 0 && this.axisX <= (BOARD_SIZE_X - 1)) {
                this.axisX -= 1;
            } else if (this.face == SOUTH && this.axisY > 0 && this.axisY <= (BOARD_SIZE_Y - 1)) {
                    this.axisY -= 1;
            } else {
                throw new RobotException(this.complaint());
            }
        },
        complaint : function () {
            return "No way! I will fall";
        }
    }
    return robot;
}
