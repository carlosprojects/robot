global.EAST = 0;
global.NORTH = 90;
global.WEST = 180;
global.SOUTH = 270;


global.BOARD_SIZE_X = 5;
global.BOARD_SIZE_Y = 5;


var faceNumberToObjectMap = new Map();
faceNumberToObjectMap.set(EAST, {
    degrees : EAST,
    label : 'EAST'
});
faceNumberToObjectMap.set(NORTH, {
    degrees : NORTH,
    label : 'NORTH'
});
faceNumberToObjectMap.set(WEST, {
    degrees : WEST,
    label : 'WEST'
});
faceNumberToObjectMap.set(SOUTH, {
    degrees : SOUTH,
    label : 'SOUTH'
});

global.FACE_NUMBER_TO_OBJECT_MAP = faceNumberToObjectMap;


var faceStringToNumberMap = new Map();
faceStringToNumberMap.set("NORTH", NORTH);
faceStringToNumberMap.set("SOUTH", SOUTH);
faceStringToNumberMap.set("EAST", EAST);
faceStringToNumberMap.set("WEST", WEST);

global.FACE_STRING_TO_NUMBER_MAP = faceStringToNumberMap;
