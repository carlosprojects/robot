/**
 * Application common validations.
 */

/**
 * InvalidArgumentException Exception.
 *
 * @param {String} Error message.
 */
InvalidArgumentException = function(message) {
    this.message = message;
    this.name = "InvalidArgumentException";
};

/**
 * Validates aurguments.
 *
 * @param {Object} arguments object.
 */
exports.arePlaceArgumentsValid = function(args) {

    if (isNaN(args.x)) {
        throw new InvalidArgumentException("Argument X is not a valid integer");
    }
    if (isNaN(args.y)) {
        throw new InvalidArgumentException("Argument Y is not a valid integer");
    }
    if (typeof args.f !== 'string') {
        throw new InvalidArgumentException("Argument F is not a String");
    } else {
        var validFaces = ["NORTH", "SOUTH", "EAST", "WEST"];
        if (validFaces.indexOf(args.f.toUpperCase()) == -1) {
            throw new RobotException("Robot can only be placed looking NORTH, SOUTH, EAST, WEST.");
        }
    }
}
