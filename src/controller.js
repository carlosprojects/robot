/**
 * Toy Robot Controller
 */
 var constants = require('./constants.js');
 var validations = require('./input-validations.js');
 var robot = require('./robot.js');


/**
 * Extract arguments from PLACE command.
 *
 * @param {String} PLACE command line. E.G. "PLACE 2,2,SOUTH"
 */
function getPlaceArguments(cmd) {
    var argsArray = cmd.replace(/PLACE |\r|\r\n|\n\r/g, "").split(",");
    var args = {};
    if (argsArray.length == 3) {
        args.x = argsArray[0];
        args.y = argsArray[1];
        args.f = argsArray[2];
    }
    validations.arePlaceArgumentsValid(args);
    return args;
}

exports.execute = function(commandsArray) {

    var r;
    var report = {
        commands : [],
        position : "",
        error : ""
    };
    commandsArray.forEach(function (cmd) {

        try {

            cmd = cmd.toUpperCase();
            if (r == undefined) {
                if (cmd.startsWith("PLACE")) {
                    var args = getPlaceArguments(cmd);
                    var x = Number(args.x);
                    var y = Number(args.y);
                    var f = FACE_STRING_TO_NUMBER_MAP.get(args.f.toUpperCase());
                    r = robot.getRobot(x, y, f);

                    report.commands.push(cmd);
                }
            } else {
                if (cmd.startsWith("PLACE")) {
                    var args = getPlaceArguments(cmd);
                    var x = Number(args.x);
                    var y = Number(args.y);
                    var f = FACE_STRING_TO_NUMBER_MAP.get(args.f.toUpperCase());
                    r.place(x, y, f);

                    report.commands.push(cmd);
                } else if (cmd.startsWith("MOVE")) {
                    try {
                        r.move();

                        report.commands.push(cmd);
                    } catch(e) {
                        //console.log(e.message);
                    }
                } else if (cmd.startsWith("RIGHT")) {
                    r.right();

                    report.commands.push(cmd);
                } else if (cmd.startsWith("LEFT")) {
                    r.left();

                    report.commands.push(cmd);
                } else if (cmd.startsWith("REPORT")) {
                    console.log(r.report());

                    report.commands.push(cmd);
                } else  {
                    //console.log("Unsupported command: " + line);
                }
            }
        } catch(e) {
            report.error = e.message;
        }
    });

    if (r != undefined) {
        report.position = r.report();
    }
    return report;
}
